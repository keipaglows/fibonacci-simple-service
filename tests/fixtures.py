import pytest

from falcon import testing
from mock import patch

from fibonacci_simple_service.app import make_app

from .mockredis import mock_redis_client


def get_mock_redis_client():
    redis_client = mock_redis_client()
    redis_client.mset({0: 0, 1: 1})

    return redis_client


@pytest.fixture
def client():
    return testing.TestClient(make_app({}))


@pytest.fixture
def redis_client(client):
    redis_patcher = patch(
        'fibonacci_simple_service.resources.fibonacci.get_redis_client',
        get_mock_redis_client
    )

    redis_patcher.start()
    yield
    redis_patcher.stop()


@pytest.fixture
def test_data(client):
    return dict(
        full_query_response={"sequence": [55, 89, 144, 233, 377, 610]},
        single_query_response={"sequence": [5]},
        wrong_query_response={"sequence": []}
    )
