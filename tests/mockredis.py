from mockredis import MockRedis


# сохраняем int в MockRedis как есть, для симулирования set_response_callback у клиента Redis
class FSSMockRedis(MockRedis):

    def _encode(self, value):
        return value


def mock_redis_client(**kwargs):
    return FSSMockRedis()
