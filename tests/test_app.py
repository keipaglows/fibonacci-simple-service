from falcon import HTTP_OK


def test_full_query_success(client, redis_client, test_data):
    response = client.simulate_get('/fibonacci', query_string='from=10&to=15')

    assert response.json == test_data['full_query_response']
    assert response.status == HTTP_OK


def test_single_query_success(client, redis_client, test_data):
    response = client.simulate_get('/fibonacci', query_string='from=5&to=5')

    assert response.json == test_data['single_query_response']
    assert response.status == HTTP_OK


def test_wrong_order_query_success(client, redis_client, test_data):
    response = client.simulate_get('/fibonacci', query_string='from=15&to=10')

    assert response.json == test_data['wrong_query_response']
    assert response.status == HTTP_OK
