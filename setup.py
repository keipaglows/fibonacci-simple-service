from setuptools import setup


install_requires = [
    'falcon',
    'redis',
    'webargs'
]

tests_require = [
    'coverage',
    'mock',
    'mockredispy',
    'pytest',
    'pytest_cov'
]

setup(
    name='fibonacci_simple_service',
    version='1.0',
    include_package_data=True,
    install_requires=install_requires,
    tests_require=tests_require,
    setup_requires=[
        'pytest-runner'
    ],
    entry_points={
        'paste.app_factory': [
            'main = fibonacci_simple_service.app:make_app'
        ]
    }
)
