# Fibonacci Simple Service

Simple service to get numbers from Fibonacci sequence

## Building with Docker

Run docker compose to start service locally on `http://127.0.0.1:5087`

```
docker-compose up
```

Optionally you can access it on `http://fss.localhost.com:8080` by configuring your `/etc/hosts` file

## Building with virtualenv and buildout

Create somewhere a Python virtual environment

```
python3 -m venv env / virtualenv -p python3 env
```

Upgrade packaging tools and install `zc.buildout`

```
env/bin/pip install --upgrade pip setuptools
env/bin/pip install zc.buildout
```

Buildout project

```
env/bin/buildout
```

To start service on `http://127.0.0.1:5087` run

```
./start.sh
```

Optionally you can access it on `http://fss.localhost.com` by configuring your `/etc/hosts` file and symlinking `nginx.cfg` to your local nginx configuration

## Running tests

After project buildout you can run tests like this

```
./bin/py setup.py test
```
