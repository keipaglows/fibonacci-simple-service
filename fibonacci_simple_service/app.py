import configparser
import falcon

from .lib.middlewares import JsonEncodeMiddleware
from .lib.redis import redis_client_factory


app = falcon.API(middleware=[
    JsonEncodeMiddleware()
])

redis_host = 'localhost'


def make_app(global_conf, **app_conf):
    from .resources.fibonacci import FibonacciCollection

    def not_found_sink(req, resp):
        raise falcon.HTTPMovedPermanently('/fibonacci')

    app.add_route('/fibonacci', FibonacciCollection())
    app.add_sink(not_found_sink, prefix='')
    app.set_error_serializer(error_serializer)

    return app


def app_factory():
    config_file = '/configuration/docker/project.ini'
    config = configparser.ConfigParser()
    config.read(config_file)

    global redis_host
    redis_host = 'redis'

    return make_app({
        '__file__': config_file
    })


def error_serializer(req, resp, exception):
    resp.body = {'errors': [exception.to_dict()]}


def get_redis_client():
    return redis_client_factory(redis_host)
