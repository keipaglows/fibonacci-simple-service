import json


class JsonEncodeMiddleware:

    def process_response(self, req, res, resource=None, req_succeeded=False):
        if res.body and not isinstance(res.body, str):
            res.body = json.dumps(res.body)
