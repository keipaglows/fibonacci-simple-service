from falcon import HTTPInternalServerError
from redis import Redis, ConnectionError


def redis_client_factory(host):
    redis_client = Redis(host=host, port=6379)

    try:
        redis_client.ping()
    except ConnectionError:
        raise HTTPInternalServerError(title='Redis service is unavailable')

    redis_client.set_response_callback('GET', lambda value: value and int(value))
    # устанавливаем первые два значения из ряда Фибоначчи если их нет
    redis_client.msetnx({0: 0, 1: 1})

    return redis_client
