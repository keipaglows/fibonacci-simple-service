from falcon import HTTPInternalServerError
from marshmallow.validate import Range
from webargs import fields
from webargs.falconparser import use_args

from fibonacci_simple_service.app import get_redis_client


class FibonacciCollection:

    validate_args = {
        'from': fields.Integer(validate=Range(min=0), missing=0),
        'to': fields.Integer(validate=Range(min=0), missing=10)
    }

    @use_args(validate_args)
    def on_get(self, req, resp, validated_args):
        resp.body = {
            'sequence': self.generate_sequence(validated_args['from'], validated_args['to'] + 1)
        }

    def generate_sequence(self, *args):
        try:
            return [self.get_fibonacci_number(number) for number in range(*args)]
        except RecursionError:
            raise HTTPInternalServerError(
                title='Recursion: Check redis service, it was probably flushed'
            )

    def get_fibonacci_number(self, number):
        redis_client = get_redis_client()
        redis_number = redis_client.get(number)

        if redis_number is not None:
            return redis_number

        redis_client.set(number, self.get_fibonacci_number(number - 1) +
                                 self.get_fibonacci_number(number - 2))

        return redis_client.get(number)
